/**
 * Class Monitor
 * To synchronize dining philosophers.
 *
 * @author Serguei A. Mokhov, mokhov@cs.concordia.ca
 */
public class Monitor
{
	/**
	 * The availability of the chopsticks around the table
	 */
	public boolean[] chopstickAvailable;
	
	/**
	 * The possibilty for philosopher to talk
	 */
	public boolean talkingAvailable;

	/**
	 * Constructor
	 */
	public Monitor(int piNumberOfPhilosophers)
	{
		chopstickAvailable = new boolean[piNumberOfPhilosophers];
		for(int i = 0; i < chopstickAvailable.length; i++)
		{
			chopstickAvailable[i] = true;
		}
		
		talkingAvailable = true;
	}

	/*
	 * -------------------------------
	 * User-defined monitor procedures
	 * -------------------------------
	 */

	/**
	 * Grants request (returns) to eat when both chopsticks/forks are available.
	 * Else forces the philosopher to wait()
	 * @throws InterruptedException 
	 */
	public synchronized void pickUp(final int piTID) throws InterruptedException
	{
		int[] chopstickIndices = selectChopsticksIndices(piTID);
		
		while(!canPickUpChopsticks(piTID, chopstickIndices))
		{
			this.wait();
		}
		
		for(int i = 0; i < chopstickIndices.length; i++)
		{
			chopstickAvailable[i] = false;
		}
	}

	/**
	 * When a given philosopher's done eating, they put the chopstiks/forks down
	 * and let others know they are available.
	 */
	public synchronized void putDown(final int piTID)
	{
		int[] chopstickIndices = selectChopsticksIndices(piTID);
		
		for(int i = 0; i < chopstickIndices.length; i++)
		{
			chopstickAvailable[i] = true;
		}
		
		this.notifyAll();
	}

	/**
	 * Only one philopher at a time is allowed to philosophy
	 * (while she is not eating).
	 */
	public synchronized void requestTalk() throws InterruptedException
	{
		while(!talkingAvailable)
		{
			this.wait();
		}
		
		talkingAvailable = false;
	}

	/**
	 * When one philosopher is done talking stuff, others
	 * can feel free to start talking.
	 */
	public synchronized void endTalk()
	{
		talkingAvailable = true;
		this.notifyAll();
	}
	
	/**
	 * Ensures that philosophers can only pick up their chopsticks if the two adjacent ones are available
	 */
	private boolean canPickUpChopsticks(int philosopherId, int[] chopstickIndices)
	{
		return chopstickAvailable[chopstickIndices[0]] && chopstickAvailable[chopstickIndices[1]];
	}
	
	/**
	 * Sets up a pair of the chopsticks' indices that are adjacent to the specified philosopher
	 */
	private int[] selectChopsticksIndices(int philosopherId)
	{
		int[] indices = new int[2];
		
		indices[0] = philosopherId - 2 < 0 ? DiningPhilosophers.DEFAULT_NUMBER_OF_PHILOSOPHERS - 1 : philosopherId - 2;
		indices[1] = philosopherId >= DiningPhilosophers.DEFAULT_NUMBER_OF_PHILOSOPHERS - 1 ? 0 : philosopherId - 1;
		
		return indices;
	}
}

// EOF
